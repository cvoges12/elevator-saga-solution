var queue = [];
var elevatorNum = 0;
var lastFloor;

function requestElevator(floors) {
    floors.map(floor => floor.on(
        "up_button_pressed down_button_pressed",
        event => {
            if (!queue.includes(floor.level))
                queue.push(floor.level);
        }
    ));
}

function reverseQueueSort(elevator) {
    if (elevator.destinationQueue[0] >= elevator.currentFloor()) {
       elevator.destinationQueue.sort(
           (a, b) => a - b // ascending
       );
       return;
    }

    elevator.destinationQueue.sort(
       (a, b) => b - a // descending
    );
}

function assignFloor(elevator, floorLevel) {
    if (!elevator.destinationQueue.includes(floorLevel)) {
        elevator.destinationQueue.push(floorLevel);
        reverseQueueSort(elevator);

        elevator.checkDestinationQueue();
    }
}

function isSameDirection(state, direction) {
    let isBothDown = state.down !== "" && direction === "down";
    let isBothUp = state.up !== "" && direction === "up";

    return isBothDown || isBothUp;
}

function passingFloor(elevator, floors, floorLevel, direction) {
    if (isSameDirection(
        floors[floorLevel].buttonStates,
        direction
    ) && elevator.loadFactor() <= 0.6) {
        if (!elevator.destinationQueue.includes(floorLevel)) {
            elevator.destinationQueue.unshift(floorLevel);

            elevator.checkDestinationQueue();
        }
    }
}

function removeFloor(floorLevel) {
    if (queue.includes(floorLevel)) {
        lastFloor = floorLevel;
        queue.splice(queue.indexOf(floorLevel), 1);
    }
}

function idle(elevator) {
    if (queue.length > 0) {
        assignFloor(elevator, queue.shift());
    } else {
        assignFloor(elevator, 0);
    }
}

function runElevators(elevators, floors) {
    elevators.map(elevator => {
        elevatorNum++;
        elevator.num = elevatorNum;

        elevator.on(
            "floor_button_pressed",
            floorLevel => assignFloor(elevator, floorLevel)
        );

        elevator.on(
            "passing_floor",
            (floorLevel, direction) =>
                passingFloor(elevator, floors, floorLevel, direction)
        );

        elevator.on(
            "stopped_at_floor",
            removeFloor
        );

        elevator.on(
            "idle",
            () => idle(elevator)
        );
    });
}

const app = {
    init: (elevators, floors) => {
        requestElevator(floors);
        runElevators(elevators, floors);
    },

    update: (dt, elevators, floors) => {},
};

app;